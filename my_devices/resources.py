
from restop.application import ApplicationError,CriticalError
from extended_resources import SimpleAgentResource
from models import MyDevice

from core import MyAgent


#from restop.plugins.resources import PoolAgentResource




#class MyDeviceResource(JsonResource):
#class MyDeviceResource(PoolAgentResource):
class MyDeviceResource(SimpleAgentResource):
    """



    """
    collection= 'my_devices'


    def op_start(self,item,**kwargs):
        """

        start the thread which launch and control the executable
        :param item:
        :param kwarg:
        :return:
        """
        agent= MyAgent.start(item,'../my_devices/executable.py',self.backend)
        return True

    def op_stop(self,item,**kwargs):
        """

        :param item:
        :param kwarg:
        :return:
        """
        agent=MyAgent(item,backend=self.backend)
        agent.stop()
        return dict(status=200,message='OK',logs=["OK"])


    def op_say(self,agent_id,message):
        """


        :param message:
        :return:
        """

        agent = MyDevice.get(agent_id,self.backend)

        device_id = agent.attribute('parameters')['device_id']

        print "agent %s say %s" % ( device_id, message)


        response_data= dict(status=200,message='OK',logs=["no logs"])
        return response_data


    def op_tell(self, agent_id, message):
        """


        :param message:
        :return:
        """

        print message


        raise ApplicationError('no tell allowed',504,payload="a payload")

        #response_data = dict(status=200, message='OK', logs=["no logs"])
        #return response_data

    def op_ping(self,agent_id):
        """

        :param agent_id:
        :return:
        """
        agent=MyAgent(agent_id,backend=self.backend)
        agent.ping()
        response_data = dict(status=200, message='OK', logs=["no logs"])
        return response_data


    def op_read(self,agent_id,**kwargs):
        """

        :param agent_id:
        :return:
        """

        agent = MyAgent(agent_id, backend=self.backend)
        r= agent.read()
        response_data = dict(status=200, message=r, logs=["no logs"])
        return response_data
