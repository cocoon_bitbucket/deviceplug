
from qadapters.adapter import ThreadedAdapter,ClientAdapter
from models import MyDevice


class MyAgent(object):
    """


    """
    def __init__(self,agent_id,backend,**kwargs):
        """

        :param agent_id:
        :param backend:
        :param kwargs:
        """
        self.agent_id= agent_id
        self.backend = backend
        self.kwargs=kwargs

        self.agent=MyDevice.get(agent_id,backend)

        # proxy to executable
        self.proxy= ClientAdapter(agent_id)

        return


    @classmethod
    def start(cls,agent_id,command_line,backend):
        """

        launch the thread to control the executable

        :param agent_id:
        :param backend:
        :return:
        """
        driver= ThreadedAdapter(agent_id,command_line)
        driver.start()

        return

    def stop(self):
        """

        :return:
        """
        #proxy= ClientAdapter(self.agent_id)
        self.proxy.exit()

    def ping(self):
        """

        :return:
        """
        self.proxy.send('ping')
        return

    def read(self):
        """

        :return:
        """
        r= self.proxy.read()
        return r