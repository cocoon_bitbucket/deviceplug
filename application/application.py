__author__ = 'cocoon'


from flask import request

from restop.application import get_application, start

# backend
from platform_config.platform_backend import PlatformBackend as Backend


from restop.collection import GenericCollectionWithOperationApi

# import resources
import resources


#
# create flask app
#

# get a flask application
app = get_application(__name__,with_error_handler=True)

app.config.from_pyfile('config.cfg')

redis_url= app.config.get('RESTOP_REDIS_URL',"")

#backend= Backend(redis_url,app.config['WORKSPACE_ROOT'],spec_base= app.config["SPEC_ROOT"],config=app.config)
backend= Backend.new(redis_url,
                     workspace_base=app.config['WORKSPACE_ROOT'],
                     spec_base= app.config["SPEC_ROOT"],
                     config=app.config)



# for debug: erase database
backend.db.flushdb()

app.config['backend'] = backend


# load new platform into platforms/1  (default)
backend.platform_load('./platform.yml')


#
# create collection blueprint
#
blueprint_name='restop_agents'
#url_prefix= '/restop/api/v1'

url_prefix= app.config['PHONEHUB_URL_PREFIX']

# collections= [ "root", "oauth2","files","agents","pjterm_agents","droyd_agents","phone_sessions",
#                'syp_agents','droyd_phone_agents','syprunner_agents','droydrunner_agents','livebox_agents']

collections= [ "root", "oauth2","files","apidoc","platforms","phone_sessions"]


app.config['collections']= {}
app.config['collections'][blueprint_name]= collections


# add my_device
import my_devices
#collections.append('my_devices')



my_blueprint= GenericCollectionWithOperationApi.create_blueprint(blueprint_name,__name__,url_prefix=url_prefix,
                collections= collections)
app.register_blueprint(my_blueprint,url_prefix='')


# add my devices
# my_device= GenericCollectionWithOperationApi.create_blueprint('my_devices',__name__,url_prefix=url_prefix,
#                 collections= ['my_devices'])
collections.append('my_devices')


@app.route('/')
def index():
    """

    :return:
    """
    root_url=  "%s%s" % (request.base_url[:-1],app.config['PHONEHUB_URL_PREFIX'])
    return 'hello from restop server: try <a href="%s">%s</a>' % (root_url,root_url)





def start_server(**kwargs):
    """


    :param kwargs:
    :return:
    """
    start(app,**kwargs)


if __name__=="__main__":

    start_server(host="0.0.0.0",port=5000,debug=True)




