__author__ = 'cocoon'



import time
# restop imports

from restop.application import ApplicationError


from restop.plugins.resources import RootResource,PoolSessionResource


# imports for plugins
from platform_config import platform_resources


class Resource_Root(RootResource):
    """

    """
    collection= 'root'


#
#  pjsip level
#

class Resource_device_sessions(PoolSessionResource):
    """




    """
    collection= 'phone_sessions'


    def customize_item(self,session_data,session_id):
        """
            customize session: cu parameters for pjsip agents

        :param session_data:
        :param session_id:
        :return:
        """

        # new config
        members= session_data['members']

        # setup configuration for pjsip
        #cnf= PjsipSessionConfiguration(config)
        session=self.backend.platform_get_session(members)

        # compute all sessions configuration one for each clollection
        agents={}
        for session_collection in session.collections():
            session_plugin= self.__class__.select_plugin(session_collection)
            collection_agents= session_plugin.get_collection_session_configuration(session,self.backend)
            agents.update(collection_agents)

        # build the final members configuration preserving initial order
        member_configuration=[]
        for alias in members:
            entry= []
            entry.append(alias)
            entry.append(agents[alias]['collection'])
            entry.append((agents[alias]['parameters']))
            member_configuration.append(entry)
        session_data['members']= member_configuration


        return session_data



    def op_get_platform_configuration(self,item,**kwargs):
        """

        """
        session_data= self.backend.session_get(item)

        # get the platform from session
        platform_name= 'default'

        platform_id=self.backend._platform_get_by_name(platform_name)
        platform_data= self.backend.platform_get(platform_id)

        return platform_data



