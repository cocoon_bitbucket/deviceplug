

#from restop.client import RestopClient
from restop_client import RestopClient

#from session_configuration import SessionConfiguration

class ProxySession(object):
    """



    """
    collection= 'phone_sessions'
    collection_operations= ['adb_devices']

    def __init__(self,base_url,client_id='anonymus',client_secret='',grant_type= "client_credentials",
                 collection=None,collection_operations=None):
        """

        :param base_url:
        :return:
        """
        self.base_url= base_url
        # create a restop http client
        self.client= RestopClient(self.base_url)

        # set authent parameters
        self.client_id=client_id
        self.client_secret= client_secret
        self.grant_type= grant_type

        collection= collection or self.collection
        self.collection= collection
        collection_operations= collection_operations or self.collection_operations
        self.collection_operations= collection_operations

        self.current_session= {}
        self.url_collection=None


    def open(self,configuration,fake=False):
        """
            open a remote http session

        :param configuration:
        :return:
        """

        # check config
        self.config= SessionConfiguration(configuration)

        # open http session
        data= {
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'grant_type': self.grant_type,
            'members': configuration
        }
        self.url_collection= self.client.url_collection(self.collection)

        r= self.client.post(self.url_collection,data=data)
        assert r.status_code == 200
        self.current_session= r.json()

        self._start_session(fake=fake)

        return

    def agent_url(self,user):
        """

            return the agent url given by the session data

        :param alias: str , Alice, Bob ...
        :return:
        """
        if user == '-' :
            # not an agent request
            return self.base_url
        try:
            agent_url= self.current_session['links'][user]
        except KeyError:
            raise RuntimeError('no such user in current session: %s' % user)
        return agent_url

    def access_token(self):
        """

        :return: the access token from the session data
        """
        return self.current_session['token']

    def _start_session(self,fake=False):
        """


        :return:
        """
        url = self.client.url_operation(self.collection, self.current_session['id'],'start')

        # dummy session
        if fake:
            data= {'pjterm':'fake'}
            r= self.client.post(url,data=data)
        else:
            r= self.client.post(url)
        assert r.status_code == 200
        data= r.json()
        #return r.status_code
        return data

    def _send_operation(self,operation, user=None,**kwargs):
        """
            send operation request to server

        :param operation:
        :param user: str ( Alice, Bob,
        :param kwargs:
        :return:
        """
        if not self.current_session:
            raise RuntimeError('no current session')

        url = self.agent_url(user) + '/' + operation
        r= self.client.post(url,data= kwargs, access_token=self.access_token())
        assert r.status_code == 200 , 'error %s' % str(r.status_code)

        data= r.json()
        return data

    def _send_collection_operation(self,operation, **kwargs):
        """
            send request to server

        :param operation:
        :param user: str ( Alice, Bob,
        :param kwargs:
        :return:
        """
        if not self.current_session:
            raise RuntimeError('no current session')

        url = self.client.url_operation(self.collection, self.current_session['id'],operation)
        r= self.client.post(url,data= kwargs)
        assert r.status_code == 200 , 'error %s' % r.status_code

        data= r.json()
        return data


    def url_for(self,collection=None,item=None,operation=None):
        """

        :param user:
        :param collection:
        :param item:
        :param operation:
        :return:
        """
        url= self.base_url
        if collection:
            url += '/%s' % collection
        if item:
            url += '/%s' % item
        if operation:
            url += '/%s' % operation
        return url


    def _get(self,operation, user=None,**kwargs):
        """
            send GET request to server

        :param operation:
        :param user: str ( Alice, Bob,
        :param kwargs:
        :return:
        """
        if user and user != '-':
            # need an agent url  like: /syprunner/1
            if not self.current_session:
                # no agent url outside a session
                raise RuntimeError('no current session')
            url = self.agent_url(user) + '/' + operation
        else:
            # we need a wild url /collection/item/operation
            url = self.url_for(collection=operation)

        r= self.client.get(url,data= kwargs, access_token=self.access_token())
        assert r.status_code == 200 , 'error %s' % str(r.status_code)

        data= r.json()
        return data





    def close(self):
        """


        :return:
        """
        if not self.current_session:
            print "no current session"
            return dict( result=204,message='no current session',logs=[])
        url = self.client.url_operation(self.collection, self.current_session['id'],'stop')
        r= self.client.post(url)
        self.current_session= None

        assert r.status_code >= 200 and r.status_code < 300
        data=r.json()

        return data


    def __getattr__(self, function_name):
        """
            wrapper to Mobile methods

        :param function_name:
        :return:
        """
        if function_name in self.collection_operations:

            def wrapper(**kwargs):
                return self._send_collection_operation(function_name,**kwargs)
        else:
            # call a function
            def wrapper(agent_id,**kwargs):
                """

                """
                return self._send_operation(function_name,agent_id,**kwargs)
        return wrapper


class FullProxy(ProxySession):
    """
        a full proxy

            open session needs a full configuration

            a list of
                user_alias, user_collection, { parameters }


    """


class SimpleProxy(ProxySession):
    """

        a simple proxy open session with only the list of user agent allias
        the category and parameters are resolved by the remote server

    """
    collection = "phone_sessions"

    def open(self, user_alias, fake=False):
        """
            open a remote http session

        :param configuration:
        :return:
        """
        assert isinstance(user_alias,(list,tuple))
        # open http session
        data= {
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'grant_type': self.grant_type,
            'members': user_alias
        }
        self.url_collection= self.client.url_collection(self.collection)

        r= self.client.post(self.url_collection,data=data)
        assert r.status_code == 200
        self.current_session= r.json()

        r= self._start_session(fake=fake)

        return r
