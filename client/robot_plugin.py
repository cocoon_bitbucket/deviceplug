"""


    syp_plugin to interface DeviceProxy server




"""

import inspect
from pprint import pprint

from http_proxy import SimpleProxy


base_url= "http://localhost:5000/restop/api/v1"


#import robot
#from robot.libraries.BuiltIn import BuiltIn

syp_ptf= dict(

    # here comes the platform configuration
)

# set droyrunner keywords
KEYWORDS= set()


class RobotFrameworkListener(object):
    """



    """
    ROBOT_LISTENER_API_VERSION = 2
    ROBOT_LIBRARY_SCOPE = 'Global'

    def __init__(self):
        """
            init pilot
        """
        self.ROBOT_LIBRARY_LISTENER = self

    def  start_suite(self,name, attributes): pass
    def  end_suite(self,name, attributes): pass
    def  start_test(self,name, attributes): pass
    def  end_test(self,name, attributes): pass
    def  start_keyword(self,name, attributes): pass
    def  end_keyword(self,name, attributes): pass
    def  log_message(self,message): pass
    def  message(self,message): pass
    def  output_file(self,path): pass
    def  log_file(self,path): pass
    def  report_file(self,path): pass
    def  debugfile(self,path): pass
    def  close(self): pass



    # def _end_test(self, name, attrs):
    #         print 'Suite %s (%s) ending.' % (name, attrs['id'])
    #         self.test_status[name.lower()] = attrs["status"]



class Pilot(RobotFrameworkListener):
    """
        syp_plugin to interface syprunner

 
    """

    _platform = syp_ptf
    _keywords = KEYWORDS
    #_dry = False

    def __init__(self):
        """
            init pilot
        """
        super(Pilot,self).__init__()
        self.test_status = {}

        self._result = ''
        self._users = {}
        self.conf = {}      # configuration of a platform version
        self.ptf = None     # ref to a SypPlatform instance
        #self.ptf = SypPlatform(self._platform)
        self._session = None
        self._dry = False

        # add own dynamic keywords
        for name, func in inspect.getmembers(Pilot, predicate=inspect.ismethod):
            if not name.startswith('_'):
                self._keywords.add(name)

    #get_lib = BuiltIn().get_library_instance('Selenium2Library')


    def get_keyword_names(self):
        """

        :return:
        """
        return list(self._keywords)


    #     # add self keywords
    #     #return [name for name in dir(self) if hasattr(getattr(self, name), 'robot_name')]
    #     #return ['serial_synch', 'stb_status', 'send_key']
    #     # add droydrunner keywords
    #     #for name,func in inspect.getmembers(self._api._mobile_factory, predicate=inspect.ismethod  ):
    #     #    if not name.startswith('_'):
    #     #        self._keywords.append(name)
    #     # add session keywords
    #     #self._keywords.extend(['open_session','close_session','adb_devices','scan_devices'])
    #     return self._keywords
    #


    def set_pilot_dry_mode(self):
        """
            set pilot to dry mode ( no action will be taken towards devices)
        """
        print "pilot: warning: you enter in dry mode"
        self._dry = True

    def setup_pilot(self,platform_name,platform_version,platform_url=None):
        """
            initialize pilot with platform configuration file
        """
        self.platform_name= platform_name
        self.platform_version= platform_version
        self.platform_url= platform_url or base_url

        print "pilot: setting up platform pilot for platform=%s , version=%s ,url=%s" % (
            platform_name,platform_version,platform_url)

        collection_operations= ['adb_devices','get_platform_configuration','ptf_get_user_data','get_sip_address']
        self.proxy= SimpleProxy(
                self.platform_url,
                collection= 'phone_sessions',
                collection_operations= collection_operations
                )

        return

    def _dry_return(self,*args,**kwargs):
        """
            return with dry mode : always OK
        """
        print "pilot: warning you are in dry mode, no operation was transmitted to syprunner"
        self._result = "OK"
        return self._result

    def open_Session(self,*users):
        """
            open a session : start session with a terminal per user specified

            Open Session Alice Bob

            -    start a session with userA=Alice , userB=Bob
        """
        print "pilot: initialisation conf = %s " % str(self.conf)
        print "pilot: Open Session with users: %s" % str(users)

        if self._dry:
            return self._dry_return()

        result= self.proxy.open(users)
        # {u'agents_count': 2, u'agent_started': 2}
        pprint(result)
        # check all agents are started
        if result.has_key('code') and result['code'] >=300:
            raise RuntimeError('cannot open session')
        if not result['agent_started'] == result['agents_count']:
            raise RuntimeError("cant start all agents")


    def get_platform_configuration(self):
        """
            Get platform configuration ( a platform.json)
        """
        result=self.proxy.get_platform_configuration()
        print "pilot: platform configuration is %s" % str(result)
        return result

    def dummy_operation(self):
        """
            A dummy operation:  do nothing
        """
        print "pilot: dummy operation :platform configuration is %s" % str(self.conf)



    def close_session(self,result=0,error=0):
        """
            close session , unregister all users and quit
        """
        print "pilot: Close Session"
        #self.proxy.close()


        #self._users = {}

        if self._dry:
            #self._session = None
            return self._dry_return()

        r= self.proxy.close()
        print r['logs']
        #
        # try :
        #     self._session.close_session(result,error)
        # except AttributeError:
        #     pass
        # self._session=None


    ### HELPERS


    # primary interface
    def _execute(self,user,command,*args,**kwargs):
        """
           the transmission belt with syp runner plugin

           all calls to syprunner plugin should pass here
        """
        if not self._dry:
            #return self._session.execute(user,command,*args,**kwargs)
            print "=> operation %s %s" % (command,str(kwargs))
            res= self.proxy._send_operation(command,user=user,**kwargs)
            return self._handle_result(res)

        else:
            return self._dry_return()

    def _get_url(self,user,command,*args,**kwargs):
        """
           the transmission belt with syp runner plugin

           all calls to syprunner plugin should pass here
        """
        if not self._dry:
            #return self._session.execute(user,command,*args,**kwargs)
            res= self.proxy._get(command,user=user,**kwargs)
            return self._handle_result(res)

        else:
            return self._dry_return()



    def _relay(self,command,*args,**kwargs):
        """
            relay a command to syprelay
        """
        if not self._dry:
            #sip_relay_func = getattr(self._session,command)
            #return sip_relay_func(*args,**kwargs)
            raise NotImplementedError
        else:
            return self._dry_return()


    def _handle_result(self,result,raise_on_error=True):
        """


        :param result: dict   { result: 200 , logs: [] , message: ""}
        :return:
        """
        status= result.get('status',200)
        message= result.get('message',None)
        print '=== status:%s' % str(status)
        print '=== message: %s' % result.get('message','No message')
        print '=== logs:'
        pprint( result.get('logs',[]))
        # check status
        if raise_on_error:
            if isinstance(status,int):
                if status >= 400:
                    raise RuntimeError("code: %s , %s" % (str(status),str(message)))
        return message



    #mapping for the agent method (  )
    def __getattr__(self, function_name):
        """
            wrapper to wild methods

        :param function_name:
        :return:
        """
        def wrapper(agent_id,**kwargs):
            """

            """
            return self._execute(agent_id,function_name,**kwargs)
        return wrapper



    def apidoc(self,collection='-',item='-'):
        """

        :param collection:
        :return:
        """
        return self._get_url('-', 'apidoc', collection=collection,item=item)



robot_plugin = Pilot



if __name__=="__main__":

    import inspect

    p= Pilot()

    # for name, func in inspect.getmembers(Pilot, predicate=inspect.ismethod):
    #     if not name.startswith('_'):
    #         Pilot._keywords.append(name)

    p.setup_pilot("demo","qualif")




    print "Done"