__author__ = 'cocoon'

import time

from client.robot_plugin import Pilot as BasePilot


hub_url= "http://localhost:5000/restop/api/v1"



class Pilot(BasePilot):
    """

        my own pilot

    """
    _keywords= set(['tell','ping','read','stop'])

    def say(self,user,message):
        """

        :param user:
        :param message:
        :return:
        """
        return self._execute(user,"say",message=message)





def basic():

    p =Pilot()

    p.setup_pilot("demo","demo_qualif",hub_url)

    res= p.open_Session('dv1','dv2')


    try:

        res= p.say('dv1',message='say hello')

        #res= p.tell('dv1', message= "tell hello" )

        time.sleep(2)

        res= p.ping('dv1')

        res= p.read('dv1')

        res=p.apidoc('my_devices')


    except RuntimeError:
        print "interrupted"

    finally:
        r= p.close_session()


    return



if __name__=="__main__":


    basic()


    print "Done."
