from restop.application import ApplicationError
from restop.plugins.resources import PoolAgentResource



class SimpleAgentResource(PoolAgentResource):
    """

    a simple agent to work in a session


    """


    def get_agent_object(self, alias, **kwargs):
        raise NotImplementedError


    #
    # Simple Agent operation method dispatcher
    #
    def _operation(self, collection, item, operation, **kwargs):
        """
            execute an operation on an item

            standard dispatcher:

                search for a op_$operation method

                or an op_col_$operation method if item is '-'

                /<collection>/-/<operation> :

        """
        if item == '-':
            # search for collection operation
            op_name = "op_col_" + operation
        else:
            # search for item operation
            op_name = "op_" + operation
        try:
            op = getattr(self, op_name)
        except AttributeError:
            # operation not defined
            # raise KeyError('no such operation')
            if item == '-':
                raise ApplicationError("invalid operation [%s] on collection [%s]" % (operation, collection), 404)
            else:
                raise ApplicationError("invalid operation [%s] on item [%s]" % (operation, item), 404)


        post_kwargs= self.request.json

        try:
            # call operation
            if post_kwargs:
                rc=  op(item, **post_kwargs)
            else:
                rc = op(item)

        except TypeError,e:
            rc= dict(status=503,message=None,logs=[e.message])

        except ApplicationError,e:
            rc= dict( status=e.status_code, message=None, logs=[e.message])

        except Exception,e:
            rc = dict(status=503, message=None, logs=[e.message])

        return rc